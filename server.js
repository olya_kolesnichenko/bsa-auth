const GameRoom = require("./server/game_room");
const CommunicationProxy = require("./server/CommunicationProxy");

const path = require('path');
const express = require('express');
const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server);
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bodyParser = require('body-parser');
const users = require('./users.json');
const texts = require('./texts.json');

require('./passport.config');

server.listen(3000);

app.use(express.static(path.join(__dirname, './client/public')));
app.use(passport.initialize());
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, './client/index.html'));
});

app.get('/game', /*passport.authenticate('jwt'),*/ function (req, res) {
    res.sendFile(path.join(__dirname, './client/game.html'));
});

app.get('/waitgame', function (req, res) {
    res.sendFile(path.join(__dirname, './client/waitgame.html'));
});

app.get('/login', function (req, res) {
    res.sendFile(path.join(__dirname, './client/login.html'));
});

app.post('/login', function (req, res) {
    const userFromReq = req.body;
    const userInDB = users.find(user => user.login === userFromReq.login);
    if (userInDB && userInDB.password === userFromReq.password) {
        const token = jwt.sign(userFromReq, SECRET_KEY, {expiresIn: '24h'});
        res.status(200).json({auth: true, token});
    } else {
        res.status(401).json({auth: false});
    }
});

const games = [];
io.on('connection', socket => {
    socket.on("joinWaitRoom", () => {
        if (isAuthorized(socket)) joinWaitRoom(socket)
    });
    socket.on("joinGameRoom", payload => {
        if (isAuthorized(socket)) {

            const {token, gameToken} = payload;

            let game = games.find((item) =>
                item.gameToken === gameToken
            );

            game.addMember(socket, token);
        }
    })
});

function joinWaitRoom(socket) {
    socket.join(ROOM_WAIT, () => {
        io.of('/').adapter.clients([ROOM_WAIT], (err, clients) => {
            if (clients.length === 1) {
                timer();
            }
        });
    });
}

function timer() {
    let sec = 10;
    let timer = setInterval(function () {
        sec--;
        if (sec < 0) {
            clearInterval(timer);
            stopTimer();
        } else {
            io.to(ROOM_WAIT).emit('waitTimerTick', {secondsToStart: sec});
        }
    }, 1000);
}

function stopTimer() {
    io.of('/').adapter.clients(['wait'], (err, clients) => {
        const count = clients.length;
        const roomsCount = Math.ceil(count / DEFAULT_USERS_COUNT_IN_GAME) + 1;
        if (count === 1) {
            timer();
        } else if (count > 1) {
            const clientsByRooms = [];

            for (let i = 0; i < count; i++) {
                const roomId = Math.floor(i / roomsCount);
                const clientsInRoom = clientsByRooms[roomId] ? clientsByRooms[roomId] : [];
                clientsInRoom.push(clients[i]);
                clientsByRooms[roomId] = clientsInRoom;
            }

            const gameNamePrefix = new Date().getTime().toString();
            for (let i = 0; i < clientsByRooms.length; i++) {
                const clientsInRoom = clientsByRooms[i];
                const gameToken = gameNamePrefix + "___" + i;
                const communication = new CommunicationProxy(io, gameToken);
                const game = new GameRoom(communication, clientsByRooms[i].length, gameToken);
                games.push(game);
                clientsInRoom.forEach(function (client) {
                    io.sockets.connected[client].emit('startGame', {roomId: gameToken});

                });
            }
        }
    });
}

function isAuthorized(socket) {
    let authorized = false;
    try {
        const token = socket.handshake.query.token;
        if (token !== null && jwt.verify(token, SECRET_KEY)) {
            authorized = true;
        }
    } catch (err) {
    }
    if (!authorized) socket.leaveAll();
    return authorized
}
//TODO remove before release
function defaultGameRoom() {
    games.push(new GameRoom(new CommunicationProxy(io, "1"), 2, "1"))
}
defaultGameRoom();

const DEFAULT_USERS_COUNT_IN_GAME = 4;
const ROOM_WAIT = "wait";
const SECRET_KEY = "bndhbhbfdbhjvhjzcuyehjkrmnbvzdjfygewgjsdbcnbzxbcdmne";