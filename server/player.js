'use strict';
const jwt = require('jsonwebtoken');

class Player {
    constructor(token, socket, text = "") {
        this.token = token;
        this.text = text;
        this.started = new Date().getMilliseconds();
        this.socket = socket;
    }

    setProgress(progress) {
        if (progress >= 99.99) this.finish();
        this.progress = progress;
    }

    getProgress() {
        return this.progress !== undefined ? this.progress : 0;
    }

    finish() {
        if (!this.finished)
            this.finished = new Date().getMilliseconds();
    }

    isFinished() {
        return !!this.finished;
    }

    getName() {
        return jwt.decode(this.token).login
    }

    getPlayTime() {
        return this.finished - this.started;
    }

    toMap() {
        return {
            "name": this.getName(),
            "progress": this.getProgress()
        }
    }
}

module.exports = Player;