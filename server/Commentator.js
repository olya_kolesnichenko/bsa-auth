'use strict';

class Commentator {
    constructor() {
    }

    getName(){
    }

    getHelloMessage() {
    }

    getWelcomeNewPlayerMessage(){
    }

    getStartGameMessage() {
    }

    getHalfMinuteMessage() {
    }

    get30SignMessage() {
    }

    getOnFinishMessage() {
    }

    getEndGameMessage() {
    }

    getGoodByeMessage() {
    }


}

module.exports = Commentator;