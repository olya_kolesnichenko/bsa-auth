'use strict';

const Commentator = require("./Commentator");

class DarthVader extends Commentator {
    constructor(){
        super();
        this.name = 'vader';
    }

    getName(){
        return this.name;
    }

    getHelloMessage(){
        return 'Лорд Вейдер приветствует вас! Я что-то чувствую. Присутствие чего-то, что я не ощущал уже с тех пор…'
    }
    getWelcomeNewPlayerMessage(player){
        return `К нам присоединился ${player}. Император передает свои приветствия участнику!`
    }
    getStartGameMessage(players){
        return `Игра стартует! Участвуют: ${players.join(', ')} Император не разделяет Вашей оптимистической оценки сложившейся ситуации.`
    }
    getHalfMinuteMessage(lastPlayer){
        return `Используй силу, ${lastPlayer} ты пока последний!`
    }
    get30SignMessage(player){
        return `${player} за 30 символов до финиша... Ты недооцениваешь силу Темной стороны.`
    }
    getOnFinishMessage(player){
        return `${player}, я твой отец! А может быть и нет... Но ты на финише! Ты - молодец!`
    }
    getEndGameMessage(players){
        return `Игра окончена. Нет никакой борьбы. Переходи на тёмную сторону, ${players.slice(0, 1).join('')}, нам такие нужны! За ним следуют: ${players.slice(1).join(', ')} `
    }
    getGoodByeMessage(){
        return 'Если ты не со мной - значит, ты против меня! NOOOOOOOOOOOPE! Я срочно улетаю!'
    }
}

module.exports = DarthVader;