'use strict';

const jwt = require('jsonwebtoken');
const texts = require('../texts.json');
const Player = require("./player.js");
const CommentaryBox = require("./commentaryBox.js");

class GameRoom {
    constructor(communication,  playersCount, gameToken) {
        this.communication = communication;
        this.playersCount = playersCount;
        this.gameToken = gameToken;
        this.currentTime = 0;

        this.level = 1;// TODO to random

        this.commentaryBox = new CommentaryBox(communication);


        this.players = [];
    }

    startGame() {
        const self = this;
        this.currentTime = 0;
        let sec = GAME_DURATION;

        return setInterval(function () {
            sec--;
            self.currentTime = sec;
            if (sec < 0) {
                self.stopGame();
            } else {
                self.communication.sendRoomMessage('gameTimerTick', {total: GAME_DURATION, remain: sec});
                self.commentaryBox.changeProgress(self.getPlayersProgress(), GAME_DURATION, sec);
            }
        }, 1000);
    }

    stopGame() {
        if (this.timer !== undefined) {
            clearInterval(this.timer);
            this.timer = undefined;
        }

        this.communication.sendRoomMessage('stopGame', {result: this.playersToMap(this.getPlayersProgress())});

        this.commentaryBox.stopGame(this.getPlayersProgress());

    }

    textChanged(token, text) {
        const player = this.players.find((el) => el.token === token);
        player.text = text;

        const clientProgress = this.getPlayersProgress();

        const originalText = (texts.find((el) => parseInt(el["level"]) === parseInt(this.level)))['text'];

        this.communication.sendRoomMessage('progressChanged', this.playersToMap(clientProgress));

        this.communication.sendPlayerMessage(player, 'changeText', {textLen: text.length, originalLen: originalText.length, originalText});

        this.commentaryBox.changeProgress(clientProgress, GAME_DURATION, this.currentTime);

        this.commentaryBox.changeText({
            player,
            textLen: text.length,
            originalLen: originalText.length
        });
        let finishedCount = 0;
        clientProgress.forEach(function (item) {
            if (item.isFinished()) {
                finishedCount++;
            }
        });

        if (finishedCount === this.players.length)
            this.stopGame();
    }

    getProgress(typed, original) {
        return (typed.length / original.length);
    }

    addMember(socket, playerToken) {
        const self = this;
        const oldPlayer = this.players.find((el) => el.token === playerToken);
        if (oldPlayer !== undefined) {
            const index = this.players.indexOf(oldPlayer);
            this.players.splice(index, 1);
        }
        const newPlayer = new Player(playerToken, socket);
        this.players.push(newPlayer);

        socket.join(this.gameToken, () => {
            socket.on('textChanged', payload => {
                const {token, text, level} = payload;

                this.textChanged(token, text, this.level)
            });
            const originalText = (texts.find((el) => parseInt(el["level"]) === parseInt(this.level)))['text'];

            socket.emit("setUpText", {text: originalText});

            self.commentaryBox.memberJoin(self.players, newPlayer, this.players.length === this.playersCount);

            if (this.players.length === this.playersCount) {
                this.timer = this.startGame();
            }
        })
    }

    getPlayersProgress() {
        const originalText = (texts.find((el) => parseInt(el["level"]) === parseInt(this.level)))['text'];

        const self = this;

        this.players.forEach(function (item) {
            const text = item.text;
            const p = ((self.getProgress(text, originalText))*100).toFixed(1);
            item.setProgress(p);
        });
        this.players.sort(function (a, b) {
            if (b.isFinished() && a.isFinished()) {
                if (b.getPlayTime() < a.getPlayTime())
                    return 1;
                else
                    return -1;
            } else {
                if (b.getProgress() < a.getProgress())
                    return -1;
                else
                    return 1;
            }
        });

        return this.players;
    }

    playersToMap(players) {
        const result = [];

        players.forEach(function (item) {
            result.push(item.toMap())
        });
        return result;
    }
}


const GAME_DURATION = 120;

module.exports = GameRoom;