const Yoda = require("./Yoda");
const DarthVader = require("./DarthVader");

class RandomCommentorFactory {

    constructor() {
        this.commentors = [new Yoda(), new DarthVader()];
    }

    get () {
        return this.commentors[(Math.round(Math.random()))];
    }

}

module.exports = RandomCommentorFactory;