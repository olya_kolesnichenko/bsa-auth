const Yoda = require("./Yoda");
const DarthVader = require("./DarthVader");

class OrderedCommentorFactory {

    constructor() {
        this.commentors = [new Yoda(), new DarthVader()];
        this.index = 0;
    }

    get () {
        return this.commentors[this.index++ % this.commentors.length];
    }

}

module.exports = OrderedCommentorFactory;