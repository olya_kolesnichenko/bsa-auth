const OrderedCommentorFactory = require("./OrderedCommentorFactory");
const RandomCommentorFactory = require("./RandomCommentorFactory");

class CommentorFactory {

    //it can be a factory of factories :) you may change an order of Commentators
    constructor (){
        this.commentorFactory =  false ? new OrderedCommentorFactory(): new RandomCommentorFactory() ;
    }

    get() {
       return this.commentorFactory.get();
    }

}

module.exports = CommentorFactory;