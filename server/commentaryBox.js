'use strict';

const jwt = require('jsonwebtoken');
const texts = require('../texts.json');
const CommentorFactory = require("./CommentorFactory.js");

//maybe it's like a Facade...
class CommentaryBox {
    constructor(communication) {
        this.communication = communication;
        this.completedPlayers = [];
        this.commentator = new CommentorFactory();
    }

    memberJoin(players, newPlayer, all) {
        this.communication.sendPlayerMessage(newPlayer, 'commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getHelloMessage()));

        const player = newPlayer.getName();
        this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getWelcomeNewPlayerMessage(player)));

        if (all) {
            const gamePlayers = [];
            players.forEach(function (item) {
                gamePlayers.push(item.getName());
            });
            this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getStartGameMessage(gamePlayers)));
        }
    }

    changeProgress(players, gameDuration, spentTime) {
        let self = this;

        if ((gameDuration - spentTime) % 30 === 0) {
            const lastPlayer = players[players.length - 1].getName();
            this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getHalfMinuteMessage(lastPlayer)));
        }

        players.forEach(function (item) {
            if (item.getProgress() === 98) {
                this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getOnFinishMessage(item.getName())));
            }
        });
    }


    changeText(payload) {
        if ((payload.originalLen - payload.textLen) === 30) {
            const playerName = payload.player.getName();
            this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.get30SignMessage(playerName)));
        }
    }

    stopGame(players) {
        const top = players.slice(0, Math.min(3, players.length - 1));
        const topPlayers = [];
        top.forEach(function (item) {
            topPlayers.push(item.getName());
        });
        this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getEndGameMessage(topPlayers)));
        this.communication.sendRoomMessage('commentorMessage', this.getMessage(this.commentator.get(), commentator => commentator.getGoodByeMessage()));
    }

    getMessage(commentator, messageCreator) {
        return {
            "commentator": commentator.getName(),
            "message": messageCreator(commentator)
        }
    }
}

module.exports = CommentaryBox;