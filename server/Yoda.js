'use strict';

const Commentator = require("./Commentator");

class Yoda extends Commentator {
    constructor(){
        super();

        this.name = 'yoda';
    }

    getName(){
        return this.name;
    }

    getHelloMessage(){
        return 'Приветствует магистр Йода вас... Наша встреча предопределена Силой, я в этом не сомневаюсь.'
    }
    getWelcomeNewPlayerMessage(player){
        return `К нам присоединился юный подаван ${player}. Удачи джедаи желают тебе!`
    }
    getStartGameMessage(players){
        return `Стартует сейчас игра ваша. Участвуют: ${players.join(', ')} И да пребудет с Вами Сила!`
    }
    getHalfMinuteMessage(lastPlayer){
        return `Предвидеть невозможно наше будущее...  ${lastPlayer}, страх открывает доступ к темной стороне! Последний ты`
    }
    get30SignMessage(player){
        return `${player} за 30 символов до финиша... Уровень миди-хлориан в его клетках выше, чем у любого другого существа. `
    }
    getOnFinishMessage(player){
        return `Юный ${player}, почти дошел до финиша ты хе-хе.`
    }
    getEndGameMessage(players){
        return `Окончена игра ваша. Источник силы тяготения есть. ${players.slice(0, 1).join('')}, с победой поздравляю юный подаван тебя. За ним: ${players.slice(1).join(', ')} `
    }
    getGoodByeMessage(){
        return 'Да пребудет с вами Сила. Только джедай  мог победить в гонке этой... Должен над этим я поразмыслить.'
    }
}

module.exports = Yoda;