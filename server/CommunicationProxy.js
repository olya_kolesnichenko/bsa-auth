'use strict';

class CommunicationProxy {
    constructor(io, gameToken) {
        this.io = io;
        this.gameToken = gameToken;
    }

    sendRoomMessage(msg, data) {
        this.io.to(this.gameToken).emit(msg, data);
    }

    sendPlayerMessage(player, msg, data) {
        player.socket.emit(msg, data);
    }

}

module.exports = CommunicationProxy;