window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io.connect('http://localhost:3000', {query: "token=" + jwt});
        const time = document.querySelector('#time');

        const start = document.querySelector('#start');

        socket.emit('joinWaitRoom');

        socket.on('waitTimerTick', payload => {
            time.innerHTML = `${payload.secondsToStart}`;
        });

        socket.on('startGame', payload => {
            window.location.replace('/game?gameToken=' + payload.roomId);
        });


    }

}
