window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (!jwt) {
        location.replace('/login');
    } else {
        const socket = io.connect('http://localhost:3000', {query: "token=" + jwt});

        const results = document.getElementById("results");
        const textWrapper = document.getElementById("text_wrapper");
        const textField = document.getElementById("user_text");
        const timer = document.getElementById("timer");
        const commentsWrapper = document.querySelector("#comments");
        const urlParams = new URLSearchParams(window.location.search);
        const gameToken = urlParams.get('gameToken');

        let originalText = '';
        let typedText = "";

        socket.emit("joinGameRoom", {token: jwt, gameToken});

        socket.on('commentorMessage', payload => {
            createComment(payload.message, payload.commentator)
        });

        socket.on('setUpText', payload => {
            originalText = payload.text;
            textWrapper.innerHTML = originalText;

            select(0, originalText.length)
        });
        textField.addEventListener("keypress", ev => {
            const next = document.getElementById("next");
            const nextSign = next.innerHTML;

            if (ev.key === nextSign) {
                typedText += ev.key;
            }
            setTimeout(function () {
                textField.value = typedText;
            }, 10);

            socket.emit('textChanged', {text: typedText, token: jwt, level: 1});
        });

        socket.on('progressChanged', payload => {
            while (results.firstChild) {
                results.removeChild(results.firstChild);
            }

            payload.forEach(function (item) {

                const newDiv = document.createElement('div');
                const newP = document.createElement('p');
                const newProgress = document.createElement('progress');
                const newSpan = document.createElement('span');
                const newProgressBar = document.createElement('div');

                newP.setAttribute('data-value', item["progress"]);
                newP.style.width = item["progress"] + "%";
                newP.innerHTML = `${item["name"]}`;

                newProgress.classList.add('progress');
                newProgress.setAttribute('value', item["progress"]);
                newProgress.setAttribute('max', '100');

                newSpan.style.width = item["progress"] + "%";
                newSpan.innerHTML = `${item["name"]}`;

                newProgressBar.classList.add('progress-bar');
                newProgressBar.appendChild(newSpan);

                newProgress.appendChild(newProgressBar);

                newDiv.appendChild(newP);
                newDiv.appendChild(newProgress);

                results.appendChild(newDiv);
            });

        });

        socket.on('changeText', payload => {

            const {textLen, originalLen} = payload;
            select(textLen, originalLen);
        });

        socket.on('gameTimerTick', payload => {

            timer.innerHTML = `${payload.remain} sec`;
        });

        socket.on('stopGame', () => {

            timer.innerHTML = 'GAME OVER!!!';
            textField.setAttribute('disabled', true);

        });

        function select(textLen, originalLen) {
            textWrapper.innerHTML = originalText;

            let text = originalText;

            if (textLen < originalLen) {
                let beginText = text.substr(0, textLen);
                let nextSign = text.substr(textLen, 1);
                let otherText = text.substring(textLen + 1);

                textWrapper.innerHTML = '<span class="begin-text">' + beginText + '</span>' +
                    '<span class="next-text" id="next">' + nextSign + '</span>' + otherText;
            } else {
                textField.setAttribute('disabled', true);
            }
        }

        function createComment(message, commentator) {
            let newComment = document.createElement('div');
            newComment.classList.add('comment', commentator);
            newComment.innerHTML = message;
            commentsWrapper.insertBefore(newComment, commentsWrapper.children[0]);
        }
    }
};
