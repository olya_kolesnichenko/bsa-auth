window.onload = () => {

    const jwt = localStorage.getItem('jwt');
    if (jwt) {
        location.replace('/waitgame');
    } else {
        location.replace('/login');
    }
};
